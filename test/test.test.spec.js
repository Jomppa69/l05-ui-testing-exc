const excpect = require("chai").excpect;

const { expect } = require("chai");
const {Builder, By, Key, WebElement, Browser, Select} = require("selenium-webdriver");
const firefox = require("selenium-webdriver/firefox");

const BASE_URL = "https://automationintesting.com/selenium/testpage/";
const sleep = (seconds) => new Promise((resolve) => setTimeout(resolve, seconds * 1000));

describe("UI Tests", () => {

    //Creating firefox options and setting the path to firefox.exe
    /**@type {import("selenium-webdriver").ThenableWebDriver} */
    let driver = undefined;
    before(async () => {
        const options = new firefox.Options();
        options.setBinary("C:\\Program Files\\Mozilla Firefox\\firefox.exe")
    
        // Webdriver creation
        driver = await new Builder()
            .forBrowser(Browser.FIREFOX)
            .setFirefoxOptions(options)
            .build();
            await driver.get(BASE_URL);
            await sleep(1);
    });

    //Creating the tests.
    it("Can find all text boxes", async () => {
        const firstname_box = await driver.findElement(By.id("firstname"));
        const lastname_box = await driver.findElement(By.id("surname"));
    });
    it("Can find the 'Tell me more text area'.", async() => {
        const tellarea = await driver.findElement(By.css("textarea"));
    })

    it("Can find Gender select", async () => {
        const genderselect = await new Select(driver.findElement(By.id("gender")));
    });

    it("Can set name in text boxes", async () => {
        const  name = ["Joona", "Kivelä"];
        const firstname_box = await driver.findElement(By.id("firstname"));
        await firstname_box.sendKeys(...name[0]);
        
    
        const lastname_box = await driver.findElement(By.id("surname"));
        await lastname_box.sendKeys(...name[1]);

        const firstname = await firstname_box.getAttribute("value");
        const lasttname = await lastname_box.getAttribute("value");
        expect(firstname).to.eq("Joona");
        expect(lasttname).to.eq("Kivelä")
    });

    it("Can set the text in 'Tell me more text area'", async () => {
        const text ="This is extra info!";
        const tellarea = await driver.findElement(By.css("textarea"));
        await tellarea.sendKeys(...text);

        const telltext = await tellarea.getAttribute("value");
        expect(telltext).to.eq("This is extra info!")
    });

    it("Can select gender", async () => {
        const genderselect = await new Select(driver.findElement(By.id("gender")));
        await genderselect.selectByVisibleText("Male");

        const selection = await driver.findElement(By.id("gender")).getAttribute("value");
        expect(selection).to.eq("male")
        await sleep(2);
    });

    after(async () => {
        await driver.close();
    });


});